using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Org.Json;
using Android;
using Android.Graphics;
using Android.Content.Res;
using Android.Support.V7.App;

namespace Hackocracy
{
    [Activity(Label = "List of Candidates", MainLauncher = false, Icon = "@drawable/icon", Theme = "@style/Theme.AppCompat.Light.DarkActionBar")]
    public class NGOMainScreen : AppCompatActivity
    {
        private RecyclerView mRecyclerView;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.Adapter mAdapter;
        private List<Email> mEmails;

        public string ngoRegisteredID;

        protected async override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.NGOMainScreenLayout);
            ngoRegisteredID = Intent.GetStringExtra("ngoIDRegistered") ?? "1234563";
            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mEmails = new List<Email>();
            Toast.MakeText(this, "Please wait while I fetch the data", ToastLength.Long).Show();
            SignUpPosterAsync();
            //mEmails.Add(new Email() { Name = "tom", Subject = "Wanna hang out?", Message = "I'll be around tomorrow!!" });
            //mEmails.Add(new Email() { Name = "tom", Subject = "Wanna hang out?", Message = "I'll be around tomorrow!!" });
            //mEmails.Add(new Email() { Name = "tom", Subject = "Wanna hang out?", Message = "I'll be around tomorrow!!" });
            //mEmails.Add(new Email() { Name = "tom", Subject = "Wanna hang out?", Message = "I'll be around tomorrow!!" });

            //Create our layout manager
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new RecyclerAdapter(mEmails, mRecyclerView);
            mRecyclerView.SetAdapter(mAdapter);
            var IntentNGOBeggerDetailPanel = new Intent(this, typeof(NGObeggerDetailScreen));

        }

        public override void OnBackPressed()
        {
            //base.OnBackPressed();
            //Toast.MakeText(this, "Action Not Allowed!", ToastLength.Long).Show();
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }

        //public static void IntentRunner()
        //{
        //    Console.WriteLine("Fuck Yes?!");
        //    var IntentSignupButton = new Intent(this, typeof(NGObeggerDetailScreen));
        //    StartActivity(IntentSignupButton);
        //}

        private async void SignUpPosterAsync()
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://139.59.67.48");
                var result = await client.GetAsync("/helping_hands/api/BeggarInformation/beggarList.php");
                string resultContent = await result.Content.ReadAsStringAsync();

                //Console.WriteLine("This the resultContent : " + resultContent);
                //Toast.MakeText(this, "resultContent", ToastLength.Long).Show();
                //Console.WriteLine("\n----------------\n");
                try
                {
                    JSONArray array = new JSONArray(resultContent);

                    for (int i = 0; i < array.Length(); i++)
                    {
                        JSONObject json = array.GetJSONObject(i);

                        // If any record is blacklisted OR there's no address, we don't need to show it.
                        if (json.GetString("status") != "blacklisted" | string.IsNullOrEmpty(json.GetString("address")))
                        {
                            Console.WriteLine("Na na na : " + json.GetString("bId"));
                            mEmails.Add(new Email() { Name = json.GetString("address"), Subject = json.GetString("status"), Message = json.GetString("extraInformation") , BId = json.GetString("bId"), currentNgoID = ngoRegisteredID });
                            mAdapter.NotifyItemInserted(0);
                            //Toast.MakeText(this, "Added New Data", ToastLength.Long).Show();
                        }


                    }
                }
                catch (Exception Ex2)
                {

                    Console.WriteLine("Ex2 : " + Ex2);
                    Toast.MakeText(this, "Ex2", ToastLength.Long).Show();
                }
            }

        }
    }



    public class RecyclerAdapter : RecyclerView.Adapter
    {
        private List<Email> mEmails;
        private RecyclerView mRecyclerView;

        public RecyclerAdapter(List<Email> emails, RecyclerView recyclerView)
        {
            mEmails = emails;
            mRecyclerView = recyclerView;
        }

        public class MyView : RecyclerView.ViewHolder
        {
            public View mMainView { get; set; }
            public TextView mName { get; set; }
            public TextView mSubject { get; set; }
            public TextView mMessage { get; set; }
            public Spinner mStatus { get; set; }
            public ImageView mPhotoView { get; set; }

            public MyView(View view) : base(view)
            {
                mMainView = view;
                mMainView.Click += (sender, e) =>
                {
                    Console.WriteLine("Clicked!");
                };
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View row = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.row, parent, false);

            TextView txtName = row.FindViewById<TextView>(Resource.Id.txtName);
            TextView txtSubject = row.FindViewById<TextView>(Resource.Id.txtSubject);
            TextView txtMessage = row.FindViewById<TextView>(Resource.Id.txtMessage);
            //Spinner txtStatus = row.FindViewById<Spinner>(Resource.Id.spinnerBeggerStatus);
            ImageView txtImageView = row.FindViewById<ImageView>(Resource.Id.imageView1);
            //txtStatus.Prompt = "Change Person's Status";
            //txtStatus.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(txtStatus_ItemSelected);
            //var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.Status_Available, Android.Resource.Layout.SimpleSpinnerDropDownItem);
            //adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            //txtStatus.Adapter = adapter;

            MyView view = new MyView(row) { mName = txtSubject, mSubject = txtName, mMessage = txtMessage };
            return view;
        }

        private void txtStatus_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var spinner = sender as Spinner;
        }

        //public void IntentRunner()
        //{
        //    Console.WriteLine("Fuck Yes?!");
        //    var IntentSignupButton = new Intent(this, typeof(NGObeggerDetailScreen));
        //    StartActivity(IntentSignupButton);
        //}

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            MyView myHolder = holder as MyView;
            myHolder.mName.Text = mEmails[position].Name;
            myHolder.mSubject.Text = mEmails[position].Subject;
            myHolder.mMessage.Text = mEmails[position].Message;
            
            //myHolder.mMainView.Click += MMainView_Click;
            myHolder.mMainView.Click += delegate
            {
                var activity2 = new Intent(myHolder.mMessage.Context, typeof(NGObeggerDetailScreen));
                activity2.PutExtra("BId", mEmails[position].BId.ToString());
                activity2.PutExtra("ngoId", mEmails[position].currentNgoID.ToString());
                //StartActivity(activity2);
                myHolder.mMessage.Context.StartActivity(activity2);
                //myHolder.mMessage.Context.StartActivity(typeof(NGObeggerDetailScreen));
            };

        }

        private void MMainView_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Running 1");
            //int position = mRecyclerView.GetChildPosition((View)sender);
            View thisview = (View)sender;

            //int position = mRecyclerView.GetChildPosition(thisview);
            int position = mRecyclerView.GetChildLayoutPosition(thisview);
            //int position = mRecyclerView.GetChildAdapterPosition(thisview);
            Console.WriteLine("position 1 : " + position);
            Console.WriteLine("Name 1 : " + mEmails[position].Name);
            
        }


        public override int ItemCount
        {
            get { return mEmails.Count; }
        }
    }
}
