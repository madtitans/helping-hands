﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Hackocracy
{
    [Activity(MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            this.RequestWindowFeature(WindowFeatures.NoTitle);
            base.OnCreate(bundle);
            SetContentView (Resource.Layout.Main);

            var NGOButton = FindViewById<Button>(Resource.Id.buttonNGO);
            var BeggerButton = FindViewById<Button>(Resource.Id.buttonBegger);
            var hashTag = FindViewById<TextView>(Resource.Id.textViewHashtag);
            Typeface tf = Typeface.CreateFromAsset(Assets, "PoppinsMedium.ttf");
            Typeface tf1 = Typeface.CreateFromAsset(Assets, "Khula-SemiBold.ttf");
            hashTag.SetTypeface(tf, TypefaceStyle.Normal);
            NGOButton.SetTypeface(tf, TypefaceStyle.Normal);
            BeggerButton.SetTypeface(tf, TypefaceStyle.Normal);
            NGOButton.Click += NGOButton_Click;
            BeggerButton.Click += BeggerButton_Click;
        }

        private void BeggerButton_Click(object sender, System.EventArgs e)
        {
            var IntentBeggerButton = new Intent(this, typeof(BeggerLoginScreen));
            StartActivity(IntentBeggerButton);
        }

        private void NGOButton_Click(object sender, System.EventArgs e)
        {
            var IntentNGOButton = new Intent(this, typeof(NGOLoginScreen));
            StartActivity(IntentNGOButton);
        }
    }
}

