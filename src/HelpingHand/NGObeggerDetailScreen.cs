using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Graphics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;

namespace Hackocracy
{
    [Activity(Label = "Candidates List", MainLauncher = false, Theme = "@style/Theme.AppCompat.Light.DarkActionBar")]
    public class NGObeggerDetailScreen : AppCompatActivity
    {
        public string message, address, extraInformation, addedBy, status, ngoInfo, smallPhoto, message_2, ngoIdInternal;
        public bool postingStaus;
        TextView beggarAddress, beggarExtraInfo;
        ImageView beggarPhotoView;
        Button submitButton;
        Spinner spinner;

        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.NGObeggerDetails);
            string bIdInternal = Intent.GetStringExtra("BId") ?? "00";
            string ngoRegisteredId = Intent.GetStringExtra("ngoId") ?? "1234563";
            ngoIdInternal = ngoRegisteredId;
            Console.WriteLine("BID : " + bIdInternal);
            Toast.MakeText(this, "Fetching The Record Details", ToastLength.Short).Show();
            await Task.Run(() => DataFetcher(bIdInternal));

            //var beggerStatus = FindViewById<TextView>(Resource.Id.NGObeggerStatusDetail);
            Typeface tf = Typeface.CreateFromAsset(Assets, "PoppinsMedium.ttf");
            Typeface tf1 = Typeface.CreateFromAsset(Assets, "Khula-SemiBold.ttf");
            //beggerStatus.SetTypeface(tf, TypefaceStyle.Bold);
            submitButton = FindViewById<Button>(Resource.Id.beggerSubmitRecord);
            submitButton.SetTypeface(tf, TypefaceStyle.Normal);
            spinner = FindViewById<Spinner>(Resource.Id.NGObeggerStatusDetailChange);
            spinner.ItemSelected += Spinner_ItemSelected;
            spinner.Prompt = "Change the Status";
            var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.Status_Available, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            beggarAddress = FindViewById<TextView>(Resource.Id.NGObeggerAddressDetails);
            beggarExtraInfo = FindViewById<TextView>(Resource.Id.NGObeggerExtraInformationDetails);
            beggarPhotoView = FindViewById<ImageView>(Resource.Id.imageView1);
            // Create your application here
            InfoSetter();
            submitButton.Click += async delegate
            {
                // '000000' means that beggar record isn't associated with any NGO. So, it's open to change.
                // If ngoInfo and current session's ngoID are same, it's the same person and should be able to change the status.
                if (ngoInfo == "000000" | ngoInfo == ngoRegisteredId)
                {
                    string spinnerSelectedValue = spinner.SelectedItem.ToString();
                    Console.WriteLine("spinnerSelectedValue : " + spinnerSelectedValue);
                    if (spinnerSelectedValue == "NEW" | spinnerSelectedValue == "-NONE-")
                    {
                        Toast.MakeText(this, "Please Select A Valid Status", ToastLength.Short).Show(); // No need to change the status to NEW, it's by default.
                    }
                    else
                    {
                        await Task.Run(() => StatusPoster(ngoRegisteredId, bIdInternal, spinnerSelectedValue));
                        if (postingStaus)
                        {
                            Toast.MakeText(this, "Record Was Successfully Updated.", ToastLength.Short).Show(); // Success Message.
                        }
                    }
                }
                else
                {
                    Toast.MakeText(this, "Please Make Sure You're The Rightful Editor Of This Record.", ToastLength.Short).Show();
                }
            };
        }

        public void InfoSetter()
        {
            beggarAddress.Text = address;
            beggarExtraInfo.Text = extraInformation;
            var imageBitmap = GetImageBitmapFromUrl(smallPhoto);
            beggarPhotoView.SetImageBitmap(imageBitmap);
            if (ngoInfo != "000000" && ngoInfo != ngoIdInternal)
            {
                spinner.Enabled = false;
            }
            else
            {
                Console.WriteLine("Status : " + status.ToLower());
                if (status.ToLower() == "new")
                {
                    spinner.SetSelection(1);
                }
                else if (status.ToLower() == "contacting")
                {
                    spinner.SetSelection(2);
                }
                else if (status.ToLower() == "accepted")
                {
                    spinner.SetSelection(3);
                }
                else if (status.ToLower() == "blacklisted")
                {
                    spinner.SetSelection(4);
                }
                else
                {
                    Console.WriteLine("Default Case activated : " + status.ToLower());
                    spinner.SetSelection(1); // Default NEW
                }
            }

        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }

        private async Task DataFetcher(string beggarId)
        {
            //Console.WriteLine("Email Id : " + userEmail);
            //Console.WriteLine("Password : " + userPassword);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://139.59.67.48");
                var content = new FormUrlEncodedContent(new[]
                {
                //new KeyValuePair<string, string>("", "login")
                new KeyValuePair<string, string>("bId", beggarId),

            });
                var result = await client.PostAsync("/helping_hands/api/BeggarInformation/singleBeggarRecord.php", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                try
                {
                    dynamic obj2 = Newtonsoft.Json.Linq.JObject.Parse(resultContent);

                    if (obj2.error_code == 1)
                    {
                        Console.WriteLine(obj2.message);
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Here the error will be generated. "obj2.message" will have the error message.
                        this.message = obj2.message;
                    }
                    else
                    {
                        
                        this.address = obj2.address;
                        this.extraInformation = obj2.extraInformation;
                        this.addedBy = obj2.addedBy;
                        this.status = obj2.status;
                        this.ngoInfo = obj2.ngoInfo;
                        this.smallPhoto = obj2.smallPhoto;
                        //InfoSetter();
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Success Message!
                    }
                }
                catch (Exception)
                {

                    throw;
                    //Toast.MakeText(this, loginException.ToString(), ToastLength.Long).Show();
                }
            }

        }

        private async Task StatusPoster(string ngoRegistrationId, string beggerId, string statusValue)
        {
            //Console.WriteLine("Email Id : " + userEmail);
            //Console.WriteLine("Password : " + userPassword);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://139.59.67.48");
                var content = new FormUrlEncodedContent(new[]
                {
                //new KeyValuePair<string, string>("", "login")
                new KeyValuePair<string, string>("ngoRegistrationId", ngoRegistrationId),
                new KeyValuePair<string, string>("bId", beggerId),
                new KeyValuePair<string, string>("status", statusValue)

            });
                var result = await client.PostAsync("/helping_hands/api/BeggarInformation/beggarStatusUpdater.php", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                try
                {
                    dynamic obj2 = Newtonsoft.Json.Linq.JObject.Parse(resultContent);

                    if (obj2.error_code == 1)
                    {
                        Console.WriteLine(obj2.message);
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Here the error will be generated. "obj2.message" will have the error message.
                        this.message_2 = obj2.message;
                        this.postingStaus = false; // Bad Input?
                        Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Error Message!
                    }
                    else
                    {

                        this.postingStaus = true; // Flag to tell the other methods about Sign up status
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Success Message!
                    }
                }
                catch (Exception)
                {

                    throw;
                    //Toast.MakeText(this, loginException.ToString(), ToastLength.Long).Show();
                }
            }

        }

        private void Spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var spinner = sender as Spinner;
            //Toast.MakeText(this, "Choice is:"+spinner.GetItemAtPosition(e.Position),ToastLength.Short).Show();
            switch (e.Position)
            {
                case 0:
                    //Toast.MakeText(this, "Case 0 here", ToastLength.Short).Show();
                    //Do Nothing on this Case!! This is a default case.
                    break;

                case 1:
                    //Toast.MakeText(this, "Case 1 here", ToastLength.Short).Show();
                    break;

                case 2:
                    //Toast.MakeText(this, "Case 2 here", ToastLength.Short).Show();
                    break;

                case 3:
                    //Toast.MakeText(this, "Case 3 here", ToastLength.Short).Show();
                    break;

                case 4:
                    //Toast.MakeText(this, "Case 4 here", ToastLength.Short).Show();
                    break;
            }
        }
    }
}