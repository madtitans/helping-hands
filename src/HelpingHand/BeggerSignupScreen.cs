using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Support.V7.App;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;

namespace Hackocracy
{
    [Activity(Label = "BeggerLoginScreen", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class BeggerSignupScreen : AppCompatActivity
    {
        public string userFirstName, userLastName, userPhoneNumber, userAddress, userEmail, userAdharNumber, userPassword, message;
        public bool signupStatus;

        private ProgressBar spinner;

        EditText SignupBeggerFirstName;
        EditText SignupBeggerLastName;
        EditText SignupBeggerPhoneNumber;
        EditText SignupBeggerAddress;
        EditText SignupBeggerEmailID;
        EditText SignupBeggerAadhar;
        EditText SignupBeggerPassword;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BeggerMainScreenSignup);

            var SignupButton = FindViewById<Button>(Resource.Id.BeggerSignupButtonProceed);
            var hashtag3 = FindViewById<TextView>(Resource.Id.textViewHashtag3);
            SignupBeggerFirstName = FindViewById<EditText>(Resource.Id.BeggerSignupFirstName);
            SignupBeggerLastName = FindViewById<EditText>(Resource.Id.BeggerSignupLastName);
            SignupBeggerPhoneNumber = FindViewById<EditText>(Resource.Id.BeggerSignupPhoneNumber);
            SignupBeggerAddress = FindViewById<EditText>(Resource.Id.BeggerSignupAddress);
            SignupBeggerEmailID = FindViewById<EditText>(Resource.Id.BeggerSignupEmailID);
            SignupBeggerAadhar = FindViewById<EditText>(Resource.Id.BeggerSignupAadhar);
            SignupBeggerPassword = FindViewById<EditText>(Resource.Id.BeggerSignupPassword);
            spinner = FindViewById<ProgressBar>(Resource.Id.circleProgress);
            Typeface tf = Typeface.CreateFromAsset(Assets, "PoppinsMedium.ttf");
            Typeface tf1 = Typeface.CreateFromAsset(Assets, "Khula-SemiBold.ttf");
            hashtag3.SetTypeface(tf, TypefaceStyle.Normal);
            SignupButton.Click += SignupButton_ClickAsync;
        }

        private async void SignupButton_ClickAsync(object sender, EventArgs e)
        {
            //var IntentLoginButton = new Intent(this, typeof(BeggerMainScreen));
            //StartActivity(IntentLoginButton);
            if (String.IsNullOrEmpty(SignupBeggerFirstName.Text) | String.IsNullOrEmpty(SignupBeggerLastName.Text) | String.IsNullOrEmpty(SignupBeggerPhoneNumber.Text) | String.IsNullOrEmpty(SignupBeggerAddress.Text) | String.IsNullOrEmpty(SignupBeggerEmailID.Text) | String.IsNullOrEmpty(SignupBeggerAadhar.Text) | String.IsNullOrEmpty(SignupBeggerPassword.Text))
            {
                Toast.MakeText(this, "Fields Cannot Be Empty.", ToastLength.Long).Show(); // Empty fields error.
            }
            else
            {
                if (Android.Util.Patterns.EmailAddress.Matcher(SignupBeggerEmailID.Text).Matches())
                {
                    spinner.Visibility = ViewStates.Visible;

                    await Task.Run(() => SignUpPoster(SignupBeggerFirstName.Text, SignupBeggerLastName.Text, SignupBeggerPhoneNumber.Text, SignupBeggerAddress.Text, SignupBeggerEmailID.Text, SignupBeggerAadhar.Text, SignupBeggerPassword.Text));


                    if (this.signupStatus == true)
                    {
                        // Upon successful sign up, we move forward to next screen!
                        var intentloginbutton = new Intent(this, typeof(BeggerMainScreen));
                        StartActivity(intentloginbutton);
                        Finish();
                    }
                    else
                    {
                        Toast.MakeText(this, this.message, ToastLength.Long).Show(); //Showing Bad Connection Error
                    }

                    spinner.Visibility = ViewStates.Gone;
                }
                else
                {
                    Toast.MakeText(this, "Please Enter A Valid Email Address.", ToastLength.Long).Show();
                }
            }
        }

        private async Task SignUpPoster(string firstName, string lastName, string phoneNumber, string address, string email, string adharNumber, string password)
        {
            //Console.WriteLine("Email Id : " + userEmail);
            //Console.WriteLine("Password : " + userPassword);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://139.59.67.48");
                var content = new FormUrlEncodedContent(new[]
                {
                //new KeyValuePair<string, string>("", "login")
                // "+91-" to make the phone number in correct format
                new KeyValuePair<string, string>("userfirstName", firstName),
                new KeyValuePair<string, string>("userlastName", lastName),
                new KeyValuePair<string, string>("userphoneNumber", "+91-" + phoneNumber),
                new KeyValuePair<string, string>("useraddress", address),
                new KeyValuePair<string, string>("useremail", email),
                new KeyValuePair<string, string>("userAdharNumber", adharNumber),
                new KeyValuePair<string, string>("userpassword", password)
                
            });
                var result = await client.PostAsync("/helping_hands/api/UserRegistration/sign_up.php", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                try
                {
                    dynamic obj2 = Newtonsoft.Json.Linq.JObject.Parse(resultContent);

                    if (obj2.error_code == 1)
                    {
                        Console.WriteLine(obj2.message);
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Here the error will be generated. "obj2.message" will have the error message.
                        this.message = obj2.message;
                        this.signupStatus = false; // Bad Input?
                    }
                    else
                    {
                        this.userFirstName = obj2.firstName;
                        this.userLastName = obj2.lastName;
                        this.userPhoneNumber = obj2.phoneNumber;
                        this.userAddress = obj2.address;
                        this.userEmail = obj2.email;
                        this.userAdharNumber = obj2.adharNumber;
                        this.signupStatus = true; // Flag to tell the other methods about Sign up status
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Success Message!
                    }
                }
                catch (Exception)
                {

                    throw;
                    //Toast.MakeText(this, loginException.ToString(), ToastLength.Long).Show();
                }
            }

        }
    }
}