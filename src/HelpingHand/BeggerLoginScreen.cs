using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hackocracy
{
    [Activity(Label = "Hackocracy", Icon = "@drawable/icon", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class BeggerLoginScreen: AppCompatActivity
    {
        public string userFirstName, userLastName, userPhoneNumber, userAddress, userEmail, userAdharNumber, userPassword, message;
        public bool signinStatus;

        private ProgressBar spinner;

        EditText loginBeggerPhoneNumber;
        EditText loginBeggerPassword;
        protected override void OnCreate(Bundle bundle)
        {
            this.RequestWindowFeature(WindowFeatures.NoTitle);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.BeggerMainScreenLayout);

            var LoginButton = FindViewById<Button>(Resource.Id.BeggerLoginButton);
            var SignupButton = FindViewById<Button>(Resource.Id.BeggerSignupButton);
            var hashtag = FindViewById<TextView>(Resource.Id.textViewHashtag2);
            Typeface tf = Typeface.CreateFromAsset(Assets, "PoppinsMedium.ttf");
            Typeface tf1 = Typeface.CreateFromAsset(Assets, "Khula-SemiBold.ttf");
            SignupButton.SetTypeface(tf, TypefaceStyle.Bold);
            hashtag.SetTypeface(tf, TypefaceStyle.Normal);
            loginBeggerPhoneNumber = FindViewById<EditText>(Resource.Id.loginPhoneNumber);
            loginBeggerPassword = FindViewById<EditText>(Resource.Id.loginPassword);
            LoginButton.SetTypeface(tf, TypefaceStyle.Normal);
            spinner = FindViewById<ProgressBar>(Resource.Id.circleProgress);
            LoginButton.Click += LoginButton_ClickAsync;
            SignupButton.Click += SignupButton_Click;
        }

        private void SignupButton_Click(object sender, System.EventArgs e)
        {
            var IntentSignupButton = new Intent(this, typeof(BeggerSignupScreen));
            StartActivity(IntentSignupButton);
        }

        private async void LoginButton_ClickAsync(object sender, System.EventArgs e)
        {
            //var IntentLoginButton = new Intent(this, typeof(BeggerMainScreen));
            //StartActivity(IntentLoginButton);
            if (String.IsNullOrEmpty(loginBeggerPhoneNumber.Text) | String.IsNullOrEmpty(loginBeggerPassword.Text))
            {
                Toast.MakeText(this, "Fields Cannot Be Empty.", ToastLength.Long).Show(); // Empty fields error.
            }
            else
            {
                spinner.Visibility = ViewStates.Visible;

                await Task.Run(() => SignInPoster(loginBeggerPhoneNumber.Text, loginBeggerPassword.Text));


                if (this.signinStatus == true)
                {
                    // Upon successful sign up, we move forward to next screen!
                    var IntentLoginButton = new Intent(this, typeof(BeggerMainScreen));
                    IntentLoginButton.PutExtra("userAdharNo", userAdharNumber);
                    StartActivity(IntentLoginButton);
                    Finish();
                }
                else
                {
                    Toast.MakeText(this, this.message, ToastLength.Long).Show(); //Showing Bad Connection Error
                }

                spinner.Visibility = ViewStates.Gone;
            }
        }

        private async Task SignInPoster(string phoneNumber, string password)
        {
            //Console.WriteLine("Email Id : " + userEmail);
            //Console.WriteLine("Password : " + userPassword);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://139.59.67.48");
                var content = new FormUrlEncodedContent(new[]
                {
                //new KeyValuePair<string, string>("", "login")
                // "+91-" to make the phone number in correct format
                new KeyValuePair<string, string>("phoneNumber", "+91-" + phoneNumber),
                new KeyValuePair<string, string>("password", password)

            });
                var result = await client.PostAsync("/helping_hands/api/UserRegistration/sign_in.php", content);
                string resultContent = await result.Content.ReadAsStringAsync();

                try
                {
                    dynamic obj2 = Newtonsoft.Json.Linq.JObject.Parse(resultContent);

                    if (obj2.error_code == 1)
                    {
                        Console.WriteLine(obj2.message);
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Here the error will be generated. "obj2.message" will have the error message.
                        this.message = obj2.message;
                        this.signinStatus = false; // Bad Input?
                    }
                    else
                    {
                        this.userFirstName = obj2.firstName;
                        this.userLastName = obj2.lastName;
                        this.userPhoneNumber = obj2.phoneNumber;
                        this.userAddress = obj2.address;
                        this.userEmail = obj2.email;
                        this.userAdharNumber = obj2.adharNumber;
                        this.signinStatus = true; // Flag to tell the other methods about Sign up status
                        //Toast.MakeText(this, obj2.message, ToastLength.Long).Show(); // Success Message!
                    }
                }
                catch (Exception)
                {

                    throw;
                    //Toast.MakeText(this, loginException.ToString(), ToastLength.Long).Show();
                }
            }

        }
    }
}